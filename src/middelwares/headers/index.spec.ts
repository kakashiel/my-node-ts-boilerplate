import { responseTime } from './index';
import { koaContextMock, koaNextMock } from '../__mocks__/koa';

describe('Test header', () => {
  it('Test responseTime appare in the reader', async () => {
    await responseTime(koaContextMock, koaNextMock);
    expect(koaContextMock.set).toBeCalledTimes(1);
  });
});
