import * as Koa from 'koa';

export const koaContextMock = {
  header: {},
  set: jest.fn() as (e: { [key: string]: string | string[] }) => void,
  request: {},
  response: { headers: {} },
} as Koa.Context;

export const koaNextMock = jest.fn();
