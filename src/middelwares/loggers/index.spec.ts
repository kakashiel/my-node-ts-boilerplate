import { koaContextMock, koaNextMock } from '../__mocks__/koa';
import { errorLogger, traceLogger } from './index';
import * as log4js from 'log4js';

jest.mock('log4js', () => {
  const actual = jest.requireActual('log4js');
  return {
    ...actual,
    getLogger: jest.fn(),
  };
});

describe('Test logger', () => {
  it('Test debug logger', async () => {
    const logDebug = jest.fn();
    (log4js.getLogger as jest.Mock).mockReturnValue({ debug: logDebug });
    await traceLogger(koaContextMock, koaNextMock);
    expect(logDebug).toHaveBeenCalledTimes(3);
  });

  it('Test error logger', async () => {
    const logError = jest.fn();
    (log4js.getLogger as jest.Mock).mockReturnValue({ error: logError });
    koaNextMock.mockReturnValue(Promise.reject());
    try {
      await errorLogger(koaContextMock, koaNextMock);
    } catch (e) {
      expect(logError).toHaveBeenCalledTimes(1);
    }
  });
});
