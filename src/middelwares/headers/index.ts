import { Context, Next } from 'koa';

export const responseTime = async (ctx: Context, next: Next): Promise<void> => {
  const started = Date.now();
  await next();
  const ellapsed = Date.now() - started + 'ms';
  ctx.set('X-ResponseTime', ellapsed);
};
