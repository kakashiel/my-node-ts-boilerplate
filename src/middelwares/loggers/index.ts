import * as log4js from 'log4js';
import * as util from 'util';
import { Context, Next } from 'koa';

const getLogger = (): log4js.Logger => {
  const logger = log4js.getLogger();
  logger.level = process.env.LOGGER_LEVEL;
  return logger;
};

export const traceLogger = async (ctx: Context, next: Next): Promise<void> => {
  const logger = getLogger();
  logger.debug('==============START REQUEST======================');
  await next();
  logger.debug(util.inspect(ctx, false, null, true /* enable colors */));
  logger.debug('==============END REQUEST========================');
};

export const errorLogger = async (ctx: Context, next: Next): Promise<void> => {
  const logger = getLogger();
  try {
    await next();
  } catch (err) {
    logger.error(`Error: ${err}`);
  }
};
