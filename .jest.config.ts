import type { Config } from '@jest/types';
import baseconfig from './local_modules/my-generic-config/jest/jest.config';

export default async (): Promise<Config.InitialOptions> => {
	const baseconf = await baseconfig();
  return {
	  verbose: true,
	  ...baseconf
  };
};