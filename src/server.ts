import * as Koa from 'koa';
import { Server } from 'http';
import { responseTime } from './middelwares/headers';
import { errorLogger, traceLogger } from './middelwares/loggers';

export async function startServer(): Promise<Server> {
  const PORT = process.env.SERVER_PORT;
  const app = new Koa();
  app.use(traceLogger);
  app.use(errorLogger);
  app.use(responseTime);
  app.use(async (ctx) => {
    ctx.body = 'Hello World';
  });

  // eslint-disable-next-line no-async-promise-executor
  return await new Promise(async (resolve) => {
    const server = app.listen(PORT, async () => {
      console.log(`Port open: ${PORT}`);
      resolve(server);
    });
  });
}
