import type { Config } from '@jest/types';

export default async (): Promise<Config.InitialOptions> => {
  return {
    preset: 'ts-jest',
    modulePathIgnorePatterns: ['<rootDir>/dist/'],
    collectCoverageFrom: [
      '<rootDir>/src/**/*.{js,jsx,ts,tsx}',
      '<rootDir>/src/components/**/*.{js,jsx,ts,tsx}',
      // Generic exclusions
      '!**/__tests__/**/*',
      '!<rootDir>/dist/**/*',
      // Specific exclusions
      '!<rootDir>/src/server.ts',
      '!<rootDir>/src/index.ts',
    ],
    coverageDirectory: '<rootDir>/coverage',
    coverageReporters: ['text'],
    coverageThreshold: {
      global: {
        branches: 100,
        functions: 100,
        lines: 100,
        statements: 100,
      },
    },
  };
};
